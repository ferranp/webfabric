import os
import unittest
import tempfile
from fabric import main
import fabric

import webfabric

FABFILE = os.path.join(os.path.dirname(__file__), 'fabfile.py')

class WFTestCase(unittest.TestCase):

    def setUp(self):
        webfabric.app.config['DATABASE'] = ":memory:"
        webfabric.app.config['FABFILE'] = FABFILE
        self.app = webfabric.app.test_client()
        webfabric.init_db()
        docstring, tasks, default = main.load_fabfile(FABFILE)
        self.tasks = tasks

    def tearDown(self):
        pass

    def test_fabric_items(self):
        rv = self.app.get('/')
        for task in self.tasks.keys():
            self.assertTrue(task in rv.data)


if __name__ == '__main__':
    unittest.main()
