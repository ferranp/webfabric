from __future__ import with_statement

import time

from fabric.api import *
from fabric.contrib.files import sed
"""
Test deployments
"""

def test_1():
    """ Test 1 """
    local('echo aaaaaaa')


def test_2():
    """ Test 2 Test 2 Test 2 Test 2 Test 2 Test 2
    Test 2 Test 2 Test 2 Test 2 Test 2 Test 2
    Test 2 Test 2 Test 2 Test 2 Test 2 """
    local('locate jquery.min')


def test_3():
    for i in range(20):
        time.sleep(2)
        local('echo %i' % i)
    local('echo fin')

@hosts('devel.shotools.com')
def test_devel():
    run('ls /var/www')
    time.sleep(3)
    run('ls /var/')

    sudo('ls /root')
