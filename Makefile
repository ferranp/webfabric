#
# Makefile for hours
#
PROJECT := webfabric
#
# Default dirs
#
VENV := .venv


#
# Help command (default command)
#
.PHONY: help
help: helpdevel

.PHONY: helpdevel
helpdevel:
	@echo "Please use 'make <target>' where <target> is one of"
	@echo "  run         to run development server"
	@echo "  clean       to clean environment"
	@echo ""
#
# Development server
#
.PHONY: run
run: ${RUNDEPS}
	. $(VENV)/bin/activate && PYTHONPATH=".:${PYTHONPATH}" honcho start

#
# Cleanup
#
.PHONY: clean
clean:
	rm -ri npm_modules
	rm -ri ${VENV}
	# rm -ri data.db
	# rm -rf static/*
	# rm -ri .env

${VENV}: requirements.txt
	sort $< -o $<
	test -d $@ || virtualenv ${VENV}
	. ${VENV}/bin/activate ; pip install honcho ; pip install -r requirements.txt
	touch $@

