
drop table if exists logs;
create table logs (
  id integer primary key autoincrement,
  taskname string not null,
  user string,
  start timestamp,
  end timestamp

);
