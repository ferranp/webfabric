$(function (){
    var spinner_img = '<img src="'+ spinner + '"/>';
    var code;
    var timeout_handler;
    function refresh_output(){
        $.getJSON($('#output').data('url') + '?code=' + code, function(data){
            if (data.finished)
            {
                $('#output').html(data.out);
                $('#output').scrollTop($('#output')[0].scrollHeight);
            }
            else
            {
                $('#output').html(data.out + '\n' + spinner_img);
                $('#output').scrollTop($('#output')[0].scrollHeight);
                timeout_handler = setTimeout(refresh_output, 1000);
            }
        });
    }
    $('.task').click(function (event){
        event.preventDefault();
        var task = $(this).attr('rel');
        var url = $(this).attr('href');
        $('#output').html(spinner_img);
        $('#output-box').modal({'backdrop': true, 'show': true});
        $('#output-box .close-button').click(function () {$('#output-box').modal('hide');});
        $('#output-box').bind('hidden', function () {
            clearTimeout(timeout_handler);
        });
        $.post(url, {"task": task}, function(data) {
            code = data.code;
            timeout_handler = setTimeout(refresh_output, 1000);
        });
        return false;
    });

});
